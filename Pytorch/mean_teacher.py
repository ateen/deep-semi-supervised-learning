import random
from torchvision.datasets import SVHN
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import WeightedRandomSampler
import numpy as np
from sys import path
path.insert(0,'/home/atin/New_deployed_projects/SSL')

from SL1 import Net, test
import math

from torchvision.transforms import ToTensor, Compose, Lambda, ToPILImage, Normalize, RandomAffine, RandomHorizontalFlip
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch
import os
from timeit import default_timer as timer
from torch.autograd import Variable
to_img = ToPILImage()
CUDA = True
os.environ["CUDA_VISIBLE_DEVICES"]="3"

mean = (0.4376821 , 0.4437697 , 0.47280442)
std = (0.19803012, 0.20101562, 0.19703614)

k = 1000 #no of labelled sample



def addGaussian(I, ismulti=True):
    """Add Gaussian with noise
    input is numpy array H*W*C
    """
    ax = np.asarray(I)
    ax = ax.copy()
    shape = (32, 32)  # ax.shape[:2]
    intensity_noise = np.random.uniform(low=0, high=0.05)
    if ismulti:
        ax[:, :, 0] = ax[:, :, 0] * (
                    1 + intensity_noise * np.random.normal(loc=0, scale=1, size=shape[0] * shape[1]).reshape(shape[0],
                                                                                                             shape[1]))
    else:
        ax[:, :, 0] = ax[:, :, 0] + intensity_noise * np.random.normal(loc=0, scale=1,
                                                                       size=shape[0] * shape[1]).reshape(shape[0],
                                                                                                         shape[1])

    return ax


class Temporal_data(Dataset):

    def __init__(self, train_dataset):
        self.train_dataset = train_dataset

    def __len__(self):
        return len(self.train_dataset)

    def __getitem__(self, idx):
        return  self.train_dataset[idx], idx



def create_dataloader(train_dataset, test_dataset, batch_size, k, seed):

    '''Unlabelled images get label = -1'''
    n_sample = len(train_dataset)
    random.seed(seed)
    labelled_data_index = np.random.choice(range(n_sample), k, replace= False)
    un_labelled_data_index = list(set(range(n_sample)) - set(labelled_data_index))
    train_dataset.labels[un_labelled_data_index] = -1


    prob = np.array([k, n_sample-k])
    prob = 1 / prob
    weight = np.full(n_sample,prob[1])
    weight[labelled_data_index] = prob[0]
    sampler = WeightedRandomSampler(weight, len(weight),replacement=True)

    train_dataset = Temporal_data(train_dataset)
    train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                               batch_size=batch_size,
                                               num_workers=10,
                                               sampler= sampler)
    test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                              batch_size=batch_size,
                                              num_workers=10,
                                              shuffle=False)

    return train_loader, test_loader



def temporal_loss(out1, out2, w, labels):
    # MSE between current and temporal outputs
    def mse_loss(out1, out2):
        quad_diff = torch.sum((F.softmax(out1, dim=1) - F.softmax(out2, dim=1)) ** 2)
        return quad_diff / out1.data.nelement()

    def masked_crossentropy(out, labels):
        nbsup = len(torch.nonzero(labels >= 0))
        loss = F.cross_entropy(out, labels, size_average=False, ignore_index=-1)
        if nbsup != 0:
            loss = loss / nbsup
        return loss, nbsup

    sup_loss, nbsup = masked_crossentropy(out1, labels)
    unsup_loss = mse_loss(out1, out2)
    return sup_loss + w * unsup_loss, sup_loss, unsup_loss, nbsup


def ramp_up(epoch, max_epochs, max_val=30., mult=-5):
    if epoch == 0:
        return 0.
    elif epoch >= max_epochs:
        return max_val
    return max_val * np.exp(mult * (1. - float(epoch) / max_epochs) ** 2)


def weight_schedule(epoch, max_epochs, n_labeled, n_samples, max_val=30, mult=-5):
    max_val = max_val * (float(n_labeled) / n_samples)
    return ramp_up(epoch, max_epochs, max_val, mult)

def weight_schedule_simple(epoch, max_epochs=50, mult=-5):
    if epoch ==0:
        return 0
    elif epoch >= max_epochs:
        return 1
    else:
        return np.exp(mult * (1. - float(epoch) / max_epochs) ** 2)

def update_ema_variables(model, ema_model, alpha, global_step):
    # Use the true average until the exponential average is more correct
    alpha = min(1 - 1 / (global_step + 1), alpha)
    for ema_param, param in zip(ema_model.parameters(), model.parameters()):
        ema_param.data.mul_(alpha).add_(1 - alpha, param.data)
        # temp = alpha * ema_param.data + (1-alpha)* param.data
        # ema_param.data = temp * (1. / (1. - alpha ** (global_step + 1)))


def create_model(ema=False):
    model = Net()
    model.cuda()

    if ema:
        for param in model.parameters():
            param.detach_()

    return model

def sigmoid_rampup(current, rampup_length):
    """Exponential rampup from https://arxiv.org/abs/1610.02242"""
    if rampup_length == 0:
        return 1.0
    else:
        current = np.clip(current, 0.0, rampup_length)
        phase = 1.0 - current / rampup_length
        return float(np.exp(-5.0 * phase * phase))

def get_current_consistency_weight(epoch):
    # Consistency ramp-up from https://arxiv.org/abs/1610.02242
    return 100 * sigmoid_rampup(epoch, 30)

def train(train_loader, test_dataloader, model, ema_model, optimizer, num_epoch,n_sample, alpha = .999, max_epoch = 80):

    n_labeled = len(train_loader.dataset)

    global_step = 0
    for epoch in range(num_epoch):
        # w = weight_schedule(epoch,max_epoch,n_labeled,n_sample)
        w = get_current_consistency_weight(epoch)
        w = torch.autograd.Variable(torch.FloatTensor([w]).cuda(), requires_grad=False)

        for i, ((images, label), index) in enumerate(train_loader):
            images, label = images.cuda(), label.cuda()
            out = model(images)
            ema_out = ema_model(images).detach()
            # ema_out = ema_model(images)


            final_loss, sup_loss, unsup_loss, nbsup = temporal_loss(out, ema_out, w, label)

            # with torch.no_grad():
            #     for ema_param, param in zip(ema_model.parameters(), model.parameters()):
            #         # ema_param.data.mul_(alpha).add_(1 - alpha, param.data)
            #         temp = alpha * ema_param.data + (1 - alpha) * param.data
            #         ema_param.data = temp * (1. / (1. - alpha ** (global_step + 1)))

            update_ema_variables(model, ema_model, alpha, global_step)
            global_step +=1
            if i%200 == 0 :
                print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.4f}, sup_loss:{:.4f}, unsup_loss: {:.4f}'.
                      format(epoch, i * len(images), len(train_loader.dataset), 100. * i / len(train_loader),
                             final_loss.item(), sup_loss.item(), unsup_loss.item()))



            optimizer.zero_grad()
            final_loss.backward()
            optimizer.step()



        print ("Epoch {} finished..........................".format(epoch))
        test(model, test_dataloader)


if __name__ == "__main__":
    svhn_dataset_train = SVHN(root='/data02/Atin/DeployedProjects/SVHN', split='train',
                              transform=Compose([RandomAffine(degrees=0,translate=(.05,.05)), RandomHorizontalFlip(.5),Lambda(addGaussian),ToTensor(), Normalize(mean, std)]))
    svhn_dataset_test = SVHN(root='/data02/Atin/DeployedProjects/SVHN', split='test', download=True,
                             transform=Compose([ToTensor(), Normalize(mean, std)]))


    train_loader, test_loader = create_dataloader(svhn_dataset_train,svhn_dataset_test, 64,1000, 13)
    model = create_model()
    ema_model = create_model(ema=True)
    # optimizer = optim.Adam(model.parameters(), lr=.002, weight_decay=.00001, betas=(0.9, 0.99))
    optimizer = optim.Adam(model.parameters(), lr=.001, weight_decay=.00001)

    train(train_loader, test_loader, model, ema_model, optimizer, num_epoch=150, n_sample=len(svhn_dataset_train))













